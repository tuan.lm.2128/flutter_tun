// ignore_for_file: file_names
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PriceWidget extends StatelessWidget {
  final int price;
  final int discountRate;

  const PriceWidget({
    super.key,
    required this.price,
    required this.discountRate,
  });

  @override
  Widget build(BuildContext context) {
    if (discountRate > 0) {
      double discountPercentage = (discountRate / price) * 100;
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "${formatCurrency(price.toDouble())}₫",
            style: const TextStyle(
              color: Colors.grey,
              fontFamily: 'Arial',
              fontSize: 12,
              decoration: TextDecoration.lineThrough,
            ),
          ),
          const SizedBox(width: 5),
          Text(
            "${formatCurrency((price * (100 - discountRate) / 100).toDouble())}₫",
            style: const TextStyle(
              color: Colors.red,
              fontFamily: 'Arial',
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(width: 5),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 238, 19, 19),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Text(
              "-${discountPercentage.toStringAsFixed(0)}%",
              style: const TextStyle(
                color: Colors.white,
                fontFamily: 'Arial',
                fontSize: 9,
              ),
            ),
          ),
        ],
      );
    } else {
      return Text(
        "${formatCurrency(price.toDouble())}₫",
        style: const TextStyle(
          color: Colors.red,
          fontFamily: 'Arial',
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      );
    }
  }

  String formatCurrency(double price) {
    final formatter = NumberFormat("#,###", "vi_VN");
    return formatter.format(price);
  }
}
