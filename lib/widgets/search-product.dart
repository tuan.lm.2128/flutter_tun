// ignore_for_file: file_names

import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertest/model/product.dart';
import '../service/api.dart';

class SearchBar extends StatefulWidget {
  const SearchBar({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  late TextEditingController _searchController;
  List<Product> searchResults = [];

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  Future<void> _handleSearch(String query) async {
    if (query.isNotEmpty) {
      var response = await fetchProducts(searchName: query);
      if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);

// print('jsonResponse: ${jsonResponse}');

        List<Product> searchResults = [];
        for (var item in jsonResponse) {
          Product product = Product.fromJson(item);
          searchResults.add(product);
        }
        setState(() {
          this.searchResults = searchResults
              .where((product) =>
                  product.name.toLowerCase().contains(query.toLowerCase()))
              .toList();
        });

        print('search done $searchResults');
      } else {
        if (kDebugMode) {
          print('Error 123: ${response.statusCode}');
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            IconButton(
              icon: const Icon(Icons.search),
              onPressed: () {
                _handleSearch(_searchController.text);
              },
            ),
            Expanded(
              child: TextField(
                controller: _searchController,
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  hintText: 'Tìm kiếm...',
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.zero,
                  suffixIcon: _searchController.text.isNotEmpty
                      ? IconButton(
                          icon: const Icon(Icons.clear),
                          onPressed: () {
                            setState(() {
                              _searchController.clear();
                              searchResults.clear();
                            });
                          },
                        )
                      : null,
                ),
                onChanged: (value) {
                  if (kDebugMode) {
                    // ignore: unnecessary_brace_in_string_interps
                    print('__${value}');
                  }

                  _handleSearch(value);
                },
              ),
            ),
          ],
        ),
        // Danh sách sản phẩm tìm kiếm
        Expanded(
          child: ListView.builder(
            itemCount: searchResults.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(searchResults[index].name),
                onTap: () {
                  // Xử lý khi người dùng chọn một sản phẩm từ danh sách tìm kiếm
                },
              );
            },
          ),
        ),
      ],
    );
  }
}
