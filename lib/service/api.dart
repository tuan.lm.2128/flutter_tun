import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

// Get List Product or Search Product by name
Future<http.Response> fetchProducts({String? searchName}) {
  var url = 'http://localhost:3000/products';
  if (searchName != null) {
    url += '?name_like=$searchName';
  }
  if (kDebugMode) {
    // ignore: unnecessary_brace_in_string_interps
    print('__${url}');
  }
  return http.get(Uri.parse(url));
}


// Get Product By id
Future<http.Response> fetchProductDetail(int id) {
  var url  = 'http://localhost:3000/products?id=$id';
  return http.get(Uri.parse(url));
}
