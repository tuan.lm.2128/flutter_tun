// ignore_for_file: file_names

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertest/model/product.dart';
import 'package:fluttertest/screens/product-detail.dart';
import 'package:fluttertest/widgets/price-widget.dart';

class ProductList extends StatelessWidget {
  final List<Product> products;

  const ProductList({super.key, required this.products});

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      padding: const EdgeInsets.all(10),
      children: products.map((product) {
        return _buildProductCard(context, product: product);
      }).toList(),
    );
  }

  Widget _buildProductCard(BuildContext context, {required Product product}) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProductDetail(
              id: product.id,
            ),
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(10.0),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        child: Column(
          children: [
            _buildProductThumbnailUrl(product.thumbnailUrl),
            _buildProductName(product.name),
            _buildProductReview(product.ratingAverage, product.reviewCount,
                product.quantitySold),
            _buildProductPrice(product.price, product.discountRate),
          ],
        ),
      ),
    );
  }

  Widget _buildProductThumbnailUrl(String thumbnailUrl) {
    return Container(
      height: 130,
      width: 130,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(thumbnailUrl),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildProductName(String name) {
    const maxLength = 45;
    final trimmedName =
        name.length > maxLength ? '${name.substring(0, maxLength)}...' : name;
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        trimmedName,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 14,
          fontFamily: 'Arial',
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _buildProductReview(
      double ratingAverage, int reviewCount, int quantitySold) {
    return Row(
      children: [
        RatingBar.builder(
          initialRating: ratingAverage.toDouble(),
          minRating: 1,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          itemSize: 16,
          itemBuilder: (context, _) => const Icon(
            Icons.star,
            color: Colors.yellow,
          ),
          onRatingUpdate: (rating) {
            if (kDebugMode) {
              print(rating);
            }
          },
        ),
        const SizedBox(width: 10),
        Text(
          "($reviewCount)",
          style: const TextStyle(
            fontSize: 10,
            color: Colors.grey,
          ),
        ),
        const SizedBox(width: 10),
        Text(
          "| Đã bán $quantitySold",
          style: const TextStyle(
            fontSize: 10,
            color: Colors.grey,
          ),
        ),
      ],
    );
  }

  Widget _buildProductPrice(int price, int discountRate) {
    return PriceWidget(price: price, discountRate: discountRate);
  }
}
