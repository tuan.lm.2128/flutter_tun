import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:fluttertest/screens/shopping-cart.dart';
import 'package:fluttertest/service/api.dart';
import 'package:provider/provider.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertest/model/product.dart';
import 'package:fluttertest/model/cart.dart';
import 'package:fluttertest/widgets/price-widget.dart';
import 'package:http/http.dart' as http;

class ProductDetail extends StatelessWidget {
  final int id;

  const ProductDetail({
    super.key,
    required this.id,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<http.Response>(
      future: fetchProductDetail(id),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else {
          final List<dynamic> parsedData = json.decode(snapshot.data!.body);
          final productData = parsedData.first;

          final product = Product(
            id: productData['id'],
            name: productData['name'],
            thumbnailUrl: productData['thumbnail_url'],
            price: productData['price'],
            originalPrice: productData['original_price'],
            discountRate: productData['discount_rate'],
            ratingAverage: productData['rating_average'],
            reviewCount: productData['review_count'],
            quantitySold: productData['quantity_sold'] != null
                ? productData['quantity_sold']['value']
                : 0,
          );

          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              actions: [
                IconButton(
                  icon: const Icon(Icons.shopping_cart),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ShoppingCart(),
                      ),
                    );
                  },
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Image.network(
                    product.thumbnailUrl,
                    fit: BoxFit.cover,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          product.name,
                          style: const TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 8),
                        _buildProductReview(product.ratingAverage,
                            product.reviewCount, product.quantitySold),
                        const SizedBox(height: 8),
                        PriceWidget(
                            price: product.price,
                            discountRate: product.discountRate),
                        const SizedBox(height: 8),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        // Thêm sản phẩm vào giỏ hàng
                        Provider.of<CartModel>(context, listen: false)
                            .addToCart(product, 1);

                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text("Đã thêm vào giỏ hàng"),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .pop(); // Đóng hộp thoại
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                      // ignore: sort_child_properties_last
                      child: const Text(
                        'Thêm vào giỏ hàng',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                      ),
                    ),
                  ),
                  const SizedBox(height: 16),
                ],
              ),
            ),
          );
        }
      },
    );
  }

  Widget _buildProductReview(
      double ratingAverage, int reviewCount, int quantitySold) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            RatingBar.builder(
              initialRating: ratingAverage,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemSize: 20,
              itemBuilder: (context, _) => const Icon(
                Icons.star,
                color: Colors.yellow,
              ),
              onRatingUpdate: (rating) {
                if (kDebugMode) {
                  print(rating);
                }
              },
            ),
            Text(
              "($reviewCount)",
              style: const TextStyle(
                fontSize: 10,
                color: Colors.grey,
              ),
            ),
            const SizedBox(width: 10),
            Text(
              "| Đã bán $quantitySold",
              style: const TextStyle(
                color: Colors.grey,
                fontSize: 13,
              ),
            ),
          ],
        ),
        Row(
          children: [
            IconButton(
              icon: const Icon(Icons.link),
              onPressed: () {
                // Xử lý sự kiện khi người dùng nhấn vào nút Liên kết địa chỉ sản phẩm
              },
            ),
            IconButton(
              icon: const Icon(Icons.share),
              onPressed: () {
                // Xử lý sự kiện khi người dùng nhấn vào nút Share
              },
            ),
          ],
        ),
      ],
    );
  }
}
