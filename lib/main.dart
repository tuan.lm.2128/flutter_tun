// ignore_for_file: file_names
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertest/model/cart.dart';
import 'package:fluttertest/model/product.dart';
import 'package:fluttertest/screens/product-list.dart';
import 'package:fluttertest/screens/shopping-cart.dart';
// ignore: library_prefixes
import 'package:fluttertest/widgets/search-product.dart' as MySearch;
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'service/api.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (_) =>
          CartModel(), // Tạo một thực thể CartModel ở cấp độ cao nhất
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false, home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<http.Response> futureProducts;

  @override
  void initState() {
    super.initState();
    futureProducts = fetchProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 26, 177, 197),
        title: Container(
          height: 48.0,
          width: 500.0,
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: const MySearch.SearchBar(),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const ShoppingCart(),
                ),
              );
            },
            child: Container(
              alignment: Alignment.centerRight,
              height: 35,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: const Icon(
                Icons.shopping_cart,
                color: Colors.white,
                size: 35.0,
              ),
            ),
          ),
        ],
      ),
      body: FutureBuilder<Response>(
        future: futureProducts,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else {
            if (snapshot.data != null) {
              return ProductList(products: parseProducts(snapshot.data!));
            } else {
              return const Center(child: Text('No data available'));
            }
          }
        },
      ),
    );
  }

  List<Product> parseProducts(http.Response response) {
    List<Product> products = [];
    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);

      // print('got data $data');

      products = data.map((item) {
        return Product(
          id: item['id'],
          name: item['name'],
          thumbnailUrl: item['thumbnail_url'],
          price: item['price'],
          originalPrice: item['original_price'],
          discountRate: item['discount_rate'],
          ratingAverage: item['rating_average'],
          reviewCount: item['review_count'],
          // quantitySold: item.containsKey('quantity_sold') ? QuantitySold.fromJson(item['quantity_sold']) : QuantitySold(text: '', value: 0),
          quantitySold: 0,
        );
      }).toList();
    }

    return products;
  }
}
