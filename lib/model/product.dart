import 'package:flutter/material.dart';

class Product with ChangeNotifier {
  final int id;
  final String name;
  final String thumbnailUrl;
  final int price;
  final int originalPrice;
  final int discountRate;
  final double ratingAverage;
  final int reviewCount;
  final int quantitySold;

  Product({
    required this.id,
    required this.name,
    required this.thumbnailUrl,
    required this.price,
    required this.originalPrice,
    required this.discountRate,
    required this.ratingAverage,
    required this.reviewCount,
    required this.quantitySold,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'],
      name: json['name'],
      thumbnailUrl: json['thumbnail_url'],
      price: json['price'],
      originalPrice: json['original_price'],
      discountRate: json['discount_rate'],
      ratingAverage: json['rating_average'].toDouble(),
      reviewCount: json['review_count'],
      quantitySold: json['quantity_sold'] != null ? json['quantity_sold']['value'] : 0,
    );
  }

  @override
  String toString() {
    return 'Product {id: $id, name: $name, price: $price}';
  }
}
