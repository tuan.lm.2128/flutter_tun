import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertest/model/product.dart';

class CartModel extends ChangeNotifier {
  final List<CartItem> _items = [];

  List<CartItem> get items => _items;

  void addToCart(Product product, int quantity) {
    // print('a${product},${quantity}');

    final existingItemIndex =
        _items.indexWhere((item) => item.product.id == product.id);

    if (existingItemIndex != -1) {
      _items[existingItemIndex].quantity += quantity;
    } else {
      _items.add(CartItem(product: product, quantity: quantity));
    }

    print('after add to card, items: $_items');

    notifyListeners();
  }

  void removeFromCart(Product product) {
    _items.removeWhere((item) => item.product.id == product.id);
    notifyListeners();
  }

  void clearCart() {
    _items.clear();
    notifyListeners();
  }

  void toggleAll(bool value) {
    for (final item in _items) {
      item.isSelected = value;
    }
    notifyListeners();
  }

  void removeSelectedItems() {
    _items.removeWhere((item) => item.isSelected);
    notifyListeners();
  }

  void toggleItem(CartItem item) {
    item.isSelected = !item.isSelected;
    notifyListeners();
  }

  void increaseQuantity(CartItem item) {
    item.quantity++;
    notifyListeners();
  }

  void decreaseQuantity(CartItem item) {
    if (item.quantity > 1) {
      item.quantity--;
      notifyListeners();
    }
  }

  void buySelectedItems(BuildContext context, CartModel cart) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Đặt hàng thành công'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('OK'),
            ),
          ],
        );
      },
    );

    // Xóa hết các mục đã được chọn trong giỏ hàng
    cart.removeSelectedItems();
  }

  // @override
  // String toString() {
  //   return jsonEncode(items);
  // }
}

class CartItem {
  final Product product;
  int quantity;
  bool isSelected;

  CartItem(
      {required this.product, required this.quantity, this.isSelected = false});

  @override
  String toString() {
    return '{name: ${product.name}, quantity: $quantity}';
  }
}
